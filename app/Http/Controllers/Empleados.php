<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class Empleados extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empleados = \App\Empleados::paginate(5);
        return view ('empleados.index')->with("empleados" , $empleados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        //Seleccionar los jefes
        $jefes = \App\Empleados::where('Title' , 'like' , '%Manager')->get();

        //Selccionar  los cargos
      $cargos =  \App\Empleados::select('Title')->distinct()->get();
        return view('empleados.new')
                ->with("cargos", $cargos)
                ->with("jefes", $jefes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Crear un objeto 
          $empleado = new \App\Empleados();
            //Asignar atributos a ese objeto
            $empleado->FirstName = $request->input("nombre");
            $empleado->LastName = $request->input("apellido");
            $empleado->Title = $request->input("cargo");
            $empleado->ReportsTo = $request->input("jefe");
            $empleado->BirthDate = $request->input("nacimiento");
            $empleado->HireDate = $request->input("contrato");
            $empleado->Address = $request->input("dir");
            $empleado->City = $request->input("ciudad");
            $empleado->Email = $request->input("email");
            //Guardar el empleado
            $empleado->save();

            //Letrero de exito
            echo "Empleado gurdado correctamente";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Seleccionar el empleado con el id por el parametro
        $empleado = \App\Empleados::find($id);
        /*  var_dump($empleado);*/
        //mostrar detalles empleado en una vista
        return view('empleados.detalles')->with("empleado", $empleado);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
